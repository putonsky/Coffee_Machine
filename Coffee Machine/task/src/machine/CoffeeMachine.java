package machine;

import java.util.Scanner;

public class CoffeeMachine {
    int waterAmount;
    int milkAmount;
    int beansAmount;
    int cupsAmount;
    int cash;

    public CoffeeMachine (int waterAmount, int milkAmount, int beansAmount, int cupsAmount, int cash){
        this.waterAmount = waterAmount;
        this.milkAmount = milkAmount;
        this.beansAmount = beansAmount;
        this.cupsAmount = cupsAmount;
        this.cash = cash;

    }


    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        CoffeeMachine machine = new CoffeeMachine(400, 540, 120, 9, 550);


        String machineRun = "go";
        while (machineRun.equals("go")) {
            System.out.println("Write action (buy, fill, take, exit, remaining): ");
            String action = in.next();
            switch (action) {
                case "buy":
                    machine.buyDrink();
                    break;
                case "fill":
                    machine.fillMachine();
                    break;
                case "take":
                    machine.getCash();
                    break;
                case "exit":
                   machineRun = "Thank you, have a nice day!";
                    System.out.println(machineRun);
                    break;
                case "remaining":
                    machine.systemCheck();
                    break;
                default:
                    System.out.println("Wrong choice!");
            }
        }





    }

    public void systemCheck() {
        System.out.println("\nThe coffee machine has: ");
        System.out.println(this.waterAmount + " of water");
        System.out.println(this.milkAmount + " of milk");
        System.out.println(this.beansAmount + " of beans");
        System.out.println(this.cupsAmount + " of disposable cups");
        System.out.println(this.cash + " of money\n");
    }


    public void buyDrink() {
        Scanner in = new Scanner(System.in);
        System.out.println("What do you want to buy? 1 - espresso, 2 - latte, 3 - cappuccino, back - back to main menu: ");
        String choice = in.next();
        switch (choice) {
            case "1":
                if (this.waterAmount >= 250 && this.beansAmount >= 16 && this.cupsAmount > 0) {
                    System.out.println("I have enough resources, making you a coffee!");
                    this.waterAmount = this.waterAmount - 250;
                    this.beansAmount = this.beansAmount - 16;
                    this.cash = this.cash + 4;
                    this.cupsAmount = this.cupsAmount - 1;
                } else
                    System.out.println("Not enough ingredients, fill the machine please");
                break;
            case "2":
                if (this.waterAmount >= 350 && this.milkAmount >= 75 && this.beansAmount >= 20 && this.cupsAmount > 0) {
                    System.out.println("I have enough resources, making you a coffee!");
                    this.waterAmount = this.waterAmount - 350;
                    this.milkAmount = this.milkAmount - 75;
                    this.beansAmount = this.beansAmount - 20;
                    this.cash = this.cash + 7;
                    this.cupsAmount = this.cupsAmount - 1;
                } else
                    System.out.println("Not enough ingredients, fill the machine please");
                break;
            case "3":
                if (this.waterAmount >= 200 && this.milkAmount >= 100 && this.beansAmount >= 12 && this.cupsAmount > 0) {
                    System.out.println("I have enough resources, making you a coffee!");
                    this.waterAmount = this.waterAmount - 200;
                    this.milkAmount = this.milkAmount - 100;
                    this.cash = this.cash + 6;
                    this.beansAmount = this.beansAmount - 12;
                    this.cupsAmount = this.cupsAmount - 1;
                } else
                    System.out.println("Not enough ingredients, fill the machine please");
                break;
            case "back":
                return;
            default:
                System.out.println("Wrong choice!");

        }

    }

    public void fillMachine() {
        Scanner in = new Scanner(System.in);
        System.out.println("How many ml of water do you want to add: ");
        int water = in.nextInt();
        System.out.println("How many ml of milk do you want to add: ");
        int milk = in.nextInt();
        System.out.println("How many grams of coffee beans do you want to add: ");
        int beans = in.nextInt();
        System.out.println("How many disposable cups do you want to add: ");
        int cups = in.nextInt();


        this.waterAmount = this.waterAmount + water;
        this.milkAmount = this.milkAmount + milk;
        this.beansAmount = this.beansAmount + beans;
        this.cupsAmount = this.cupsAmount + cups;


    }

    public void getCash() {
        System.out.println("I gave you " + this.cash + " of money");
        this.cash = 0;


    }
}
